<?php

/*
 * Type : Cron Job
 * Task : Collect Minecraft Server Stats and log them into database. Then generate graphs.
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

$Cron = new Cron();

$Cron->collectStatistics();
$Cron->generateGraphs();

class Cron {

    private $timeStamp;
    private $servers = array(1, 2); // Define the ID's of the servers to keep a log of (Must manually put these in MySQL)
    private $serverList = array();

    public function __construct() {
        $this->timeStamp = time();

        // Load Servers
        $this->loadServers();
    }

    public function exportCacheToLog() {
        foreach ($this->serverList as $server) {

            // Is the $server really a Server object?
            if ($server instanceof Server) {

                // Get all the server stats in one go.
                $server->saveDataToLog();
                print("Saved cache data from " . $server->serverId . "<br />");
            } else {
                print("Server not really a server | " + var_dump($server));
            }
        }
    }

    public function collectStatistics() {
        foreach ($this->serverList as $server) {

            // Is the $server really a Server object?
            if ($server instanceof Server) {

                // Get all the server stats in one go.
                $server->refreshStatistics();

                $server->saveDataToCache();
                print("Saved cache data from " . $server->serverId . "<br />");
            } else {
                print("Server not really a server | " + var_dump($server));
            }
        }
    }

    public function generateGraphs() {
        
    }

    public function loadServers() {
        foreach ($this->servers as $serverId) {
            $Server = new Server($serverId);
            $this->serverList[] = $Server;
        }
    }

}

class Server {

    // Helper Variables
    private $MySQLi = null;
    //
    // Internal data
    public $serverId = 0;
    public $serverVersion = "1.6.4";
    //
    // Internal Connection data (localhost)
    private $internalHost = "127.0.0.1";
    private $internalPort = 25565;
    private $internalSocket;
    //
    // External Connection data (global-ip)
    private $externalHost = "127.0.0.1";
    private $externalPort = 25565;
    private $externalSocket;
    //
    // Internal Information (info produced from local connection)
    public $serverStatus = 0;
    public $serverMotd = "A Minecraft Server";
    public $serverTPS = 0;
    public $serverUptime = 0;
    public $serverIcon = "";
    public $lastConnection = 0;
    public $lastDowntime = 0;
    public $internalPing = -1;
    public $playersOnline = 0;
    public $playersMax = 0;
    public $players = array();
    //
    // External Information (info produced from outside-connection)
    public $externalPing = -1;

    public function __construct($serverId) {
        $this->serverId = $serverId;
        $this->connectMySQL();

        $this->loadServerData();

        $this->refreshStatistics();
        $this->saveDataToCache();
    }

    public function __destruct() {
        $this->disconnectMySQL();
    }

    public function refreshStatistics() {
        // Do Actual MC related stuff
        $this->collectLocalData();
        $this->collectExternalData();
        $this->collectPluginData();

        // Is the server offline?
        if ($this->serverStatus == 0) {
            $this->lastDowntime = time();
            $this->serverUptime = 0;
        } else {
            $this->serverUptime = time() - $this->lastDowntime;
        }
    }

    public function collectLocalData() {
        $this->internalSocketConnect();

        $this->serverStatus = 0;

        if (!$this->internalSocket)
            $this->internalSocketConnect();

        if (!$this->internalSocket)
            return false;

        if (preg_match('/1.7|1.8/', $this->serverVersion)) {
            // Ping Time
            $sTime = microtime(true);

            $handshake = pack('cccca*', hexdec(strlen($this->internalHost)), 0, 0x04, strlen($this->internalHost), $this->internalHost) . pack('nc', $this->internalPort, 0x01);

            socket_send($this->internalSocket, $handshake, strlen($handshake), 0);
            socket_send($this->internalSocket, "\x01\x00", 2, 0);

            socket_read($this->internalSocket, 1);

            $this->internalPing = round((microtime(true) - $sTime) * 1000);
            $this->serverStatus = 1;

            // Read Other Data
            $packetLen = $this->readPacketLength($this->internalSocket);

            if ($packetLen < 10)
                return false;

            $data = socket_read($this->internalSocket, $packetLen, PHP_NORMAL_READ);

            if (!$data)
                return false;

            $data = json_decode($data);

            $this->serverVersion = $data->version->name;
            $this->serverProtocol = $data->version->protocol;
            $this->playersOnline = $data->players->online;
            $this->playersMax = $data->players->max;

            $this->serverMotd = $data->description;
            $this->serverIcon = $data->favicon;
        }
        else {
            // Server is 1.6 or below - uses old protocol
            $sTime = microtime(true);

            socket_send($this->internalSocket, "\xFE\x01", 2, 0);
            $packetLen = socket_recv($this->internalSocket, $data, 512, 0);

            $this->internalPing = round((microtime(true) - $sTime) * 1000);

            if ($packetLen < 4 || $data[0] != "\xFF")
                return false;

            $this->serverStatus = 1;
            $this->lastConnection = time();
            $this->serverMotd = "";


            $result = explode("\x00", mb_convert_encoding(substr((String) $data, 15), 'UTF-8', 'UCS-2'));

            $this->serverMotd = $result[1];
            $this->playersOnline = $result[sizeof($result) - 2];
            $this->playersMax = $result[sizeof($result) - 1];

            $this->serverIcon = null;
            $this->serverProtocol = null;
        }

        $this->internalSocketDisconnect();
    }

    public function collectExternalData() {
        $this->externalSocketConnect();

        if (!$this->externalSocket)
            $this->externalSocketConnect();

        if (!$this->externalSocket)
            return false;

        if (preg_match('/1.7|1.8/', $this->serverVersion)) {
            // Ping Time
            $sTime = microtime(true);

            $handshake = pack('cccca*', hexdec(strlen($this->externalHost)), 0, 0x04, strlen($this->externalHost), $this->externalHost) . pack('nc', $this->externalPort, 0x01);

            socket_send($this->externalSocket, $handshake, strlen($handshake), 0);
            socket_send($this->externalSocket, "\x01\x00", 2, 0);

            socket_read($this->externalSocket, 1);

            $this->externalPing = round((microtime(true) - $sTime) * 1000);
        } else {
            // Server is 1.6 or below - uses old protocol
            $sTime = microtime(true);

            socket_send($this->externalSocket, "\xFE\x01", 2, 0);
            $packetLen = socket_recv($this->externalSocket, $data, 512, 0);

            $this->externalPing = round((microtime(true) - $sTime) * 1000);

            $this->lastConnection = time();
        }

        $this->externalSocketDisconnect();
    }

    public function collectPluginData() {
        // Communicate to the plugin
        // Get the TPS
        // Get a list of players
    }

    public function isOnline() {
        if ($this->serverStatus > 0)
            return true;
        return false;
    }

    public function saveDataToCache() {
        if ($this->MySQLi->connect_errno > 0)
            return;

        $sqlQuery = "UPDATE `servers` SET 
            `lastConnection` = '" . $this->MySQLi->escape_string($this->lastConnection) . "',
            `cacheVersion` = '" . $this->MySQLi->escape_string($this->serverVersion) . "',
            `cacheStatus` = '" . $this->MySQLi->escape_string($this->serverStatus) . "',
            `cacheMotd` = '" . $this->MySQLi->escape_string($this->serverMotd) . "',
            `cacheTPS` = '" . $this->MySQLi->escape_string($this->serverTPS) . "',
            `cacheUptime` = '" . $this->MySQLi->escape_string($this->serverUptime) . "',
            `cacheIcon` = '" . $this->MySQLi->escape_string($this->serverIcon) . "',
            `cacheInternalPing` = '" . $this->MySQLi->escape_string($this->internalPing) . "',
            `cacheExternalPing` = '" . $this->MySQLi->escape_string($this->externalPing) . "',
            `cachePlayersOnline` = '" . $this->MySQLi->escape_string($this->playersOnline) . "',
            `cachePlayersMax` = '" . $this->MySQLi->escape_string($this->playersMax) . "'
            WHERE `id`= '" . $this->MySQLi->escape_string($this->serverId) . "'";

        $this->MySQLi->query($sqlQuery);
        echo $this->MySQLi->error;
    }

    // Load data from MySQL
    private function loadServerData() {

        if ($this->MySQLi->connect_errno > 0)
            return;

        $sqlQuery = "SELECT `internalHost`,`internalPort`,`externalHost`,`externalPort` FROM `servers` WHERE `id`='" . $this->serverId . "' LIMIT 1";

        if (!$result = $this->MySQLi->query($sqlQuery))
            return;

        $row = $result->fetch_assoc();
        $this->internalHost = $row['internalHost'];
        $this->internalPort = $row['internalPort'];

        $this->externalHost = $row['externalHost'];
        $this->externalPort = $row['externalPort'];

        $result->free();
    }

    private function connectMySQL() {
        $this->MySQLi = new mysqli('localhost', 'minecraft_monito', 'ua64UYxUbaCuBvtH', 'minecraft_monitor');
        $this->MySQLi->connect();
        if ($this->MySQLi->connect_errno > 0) {
            echo "Failed to connect to MySQL:" . $this->MySQLi->connect_error;
            return false;
        }

        echo("<br />Connected to MySQL<br />");
        return true;
    }

    private function disconnectMySQL() {
        $this->MySQLi->close();
        echo("<br />Disconnected from MySQL<br />");
    }

    private function readPacketLength($socket) {
        $a = 0;
        $b = 0;
        while (true) {
            $c = socket_read($socket, 1);
            if (!$c) {
                return 0;
            }
            $c = Ord($c);
            $a |= ($c & 0x7F) << $b++ * 7;
            if ($b > 5) {
                return false;
            }
            if (($c & 0x80) != 128) {
                break;
            }
        }
        return $a;
    }

    // Socket Utility Functions

    private function internalSocketConnect() {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_connect($socket, $this->internalHost, $this->internalPort);
        $this->internalSocket = $socket;
        return $socket;
    }

    private function internalSocketDisconnect() {
        if ($this->internalSocket != null)
            socket_close($this->internalSocket);
    }

    private function externalSocketConnect() {
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_connect($socket, $this->externalHost, $this->externalPort);
        $this->externalSocket = $socket;
        return $socket;
    }

    private function externalSocketDisconnect() {
        if ($this->externalSocket != null)
            socket_close($this->externalSocket);
    }

}

?>